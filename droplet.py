from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('pages/index.html')

@app.route('/about')
def about():
    return render_template('pages/about.html')

@app.route('/locator')
def locator():
    return render_template('pages/locator.html')

if __name__ == '__main__':
    app.run()
