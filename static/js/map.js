function makemap(markergen_path) {
    // basic setup
    var mymap = L.map('map').setView([53, -1.470278], 6);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var markers = [{
        "type": "Feature",
        "properties": {
            "name": "Feed it green",
            "show_on_map": true
        },
        "geometry": {
            "type": "Point",
            "coordinates": [-2.982364, 53.404468]
        }
    }, {
        "type": "Feature",
        "properties": {
            "name": "Alternia Energy Management",
            "show_on_map": true
        },
        "geometry": {
            "type": "Point",
            "coordinates": [-3.010246, 53.379373]
        }
    }];

    L.geoJSON(markers, {
        filter: function (feature, layer) {
            return feature.properties.show_on_map;
        }
    }).addTo(mymap);
}